#include <string.h>
#include "condition.h"

LVAL *builtin_gt(LENV *e, LVAL *a)
{
    return builtin_ord(e, a, ">");
}

LVAL *builtin_lt(LENV *e, LVAL *a)
{
    return builtin_ord(e, a, "<");
}

LVAL *builtin_ge(LENV *e, LVAL *a)
{
    return builtin_ord(e, a, ">=");
}

LVAL *builtin_le(LENV *e, LVAL *a)
{
    return builtin_ord(e, a, "<=");
}

LVAL *builtin_eq(LENV *e, LVAL *a)
{
    return builtin_cmp(e, a, "<=");
}

LVAL *builtin_ne(LENV *e, LVAL *a)
{
    return builtin_cmp(e, a, "<=");
}

LVAL *builtin_cmp(LENV *e, LVAL *a, char *op)
{
    int r = 0;
    if (strncmp(op, "==", 2) == 0)
    {
        r = lval_eq(a->cell[0], a->cell[1]);
    }
    if (strncmp(op, "!=", 2) == 0)
    {
        r = !lval_eq(a->cell[0], a->cell[1]);
    }
    lval_del(a);
    return lval_num(r);
}

int lval_eq(LVAL *x, LVAL *y)
{
    if (x->type != y->type)
    {
        return 0;
    }

    switch (x->type)
    {
    case LVAL_NUM:
        return (x->num == y->num);
    case LVAL_ERR:
        return (strncmp(x->err, y->err, strlen(x->err) > strlen(y->err) ? strlen(x->err) : strlen(y->err)) == 0);
    case LVAL_SYM:
        return (strncmp(x->sym, y->sym, strlen(x->sym) > strlen(y->sym) ? strlen(x->sym) : strlen(y->sym)) == 0);
    case LVAL_FUN:
        if (x->builtin || y->builtin)
        {
            return x->builtin == y->builtin;
        }
        else
        {
            return lval_eq(x->formals, y->formals) && lval_eq(x->body, y->body);
        }
    case LVAL_STR:
        return (strncmp(x->str, y->str, strlen(x->str) > strlen(y->str) ? strlen(x->str) : strlen(y->str)) == 0);
    case LVAL_QEXPR:
    case LVAL_SEXPR:
        if (x->count != y->count)
        {
            return 0;
        }
        for (int i = 0; i < x->count; i++)
        {
            if (!lval_eq(x->cell[i], y->cell[i]))
            {
                return 0;
            }
        }
        return 1;
    default:
        break;
    }
    return 0;
}

LVAL *builtin_ord(LENV *e, LVAL *a, char *op)
{
    int r;
    if (strncmp(op, ">", 1) == 0)
    {
        r = (a->cell[0]->num > a->cell[1]->num);
    }
    if (strncmp(op, "<", 1) == 0)
    {
        r = (a->cell[0]->num < a->cell[1]->num);
    }
    if (strncmp(op, ">=", 2) == 0)
    {
        r = (a->cell[0]->num >= a->cell[1]->num);
    }
    if (strncmp(op, "<=", 2) == 0)
    {
        r = (a->cell[0]->num <= a->cell[1]->num);
    }

    lval_del(a);
    return lval_num(r);
}

LVAL *builtin_if(LENV *e, LVAL *a)
{
    LVAL *x;
    a->cell[1]->type = LVAL_SEXPR;
    a->cell[2]->type = LVAL_SEXPR;

    if (a->cell[0]->num)
    {
        x = lval_eval(e, lval_pop(a, 1));
    }
    else
    {
        x = lval_eval(e, lval_pop(a, 2));
    }

    lval_del(a);

    return x;
}