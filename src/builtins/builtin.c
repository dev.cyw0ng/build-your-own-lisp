#include "types.h"
#include "lval-ops.h"


LVAL *builtin_head(LENV *e, LVAL *a)
{
    LASSERT(a, a->count == 1, "Function 'haed' passed with too many arguments");
    LASSERT(a, a->cell[0]->type == LVAL_QEXPR, "Function 'head' passed incorrect type!");
    LASSERT(a, a->cell[0]->count != 0, "Function 'head' passed {}!");

    LVAL *v = lval_take(a, 0);
    while (v->count > 1)
    {
        lval_del(lval_pop(v, 1));
    }
    return v;
}

LVAL *builtin_tail(LENV *e, LVAL *a)
{
    LASSERT(a, a->count == 1, "Function 'haed' passed with too many arguments");
    LASSERT(a, a->cell[0]->type == LVAL_QEXPR, "Function 'head' passed incorrect type!");
    LASSERT(a, a->cell[0]->count != 0, "Function 'head' passed {}!");

    LVAL *v = lval_take(a, 0);
    lval_del(lval_pop(v, 0));
    return v;
}

inline LVAL *builtin_list(LENV *e, LVAL *a)
{
    a->type = LVAL_QEXPR;
    return a;
}

LVAL *builtin_eval(LENV *e, LVAL *a)
{
    LASSERT(a, a->count == 1, "Function 'eval' passed too many arguments!");
    LASSERT(a, a->cell[0]->type == LVAL_QEXPR, "Function 'eval' passed incorrect type!");

    LVAL *x = lval_take(a, 0);
    x->type = LVAL_SEXPR;
    return lval_eval(e, x);
}

LVAL *builtin_join(LENV *e, LVAL *a)
{
    for (int i = 0; i < a->count; i++)
    {
        LASSERT(a, a->cell[i]->type == LVAL_QEXPR, "Function with inc type");
    }

    LVAL *x = lval_pop(a, 0);
    while (a->count)
    {
        x = lval_join(x, lval_pop(a, 0));
    }

    lval_del(a);
    return x;
}


LVAL *lval_join(LVAL *x, LVAL *y)
{
    for (int i = 0; i < y->count; i++)
    {
        x = lval_add(x, y->cell[i]);
    }
    free(y->cell);
    free(y);
    return x;
}