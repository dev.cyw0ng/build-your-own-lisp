#include "types.h"
#include "lval-ops.h"
#include <stdbool.h>

extern mpc_parser_t *Lispy;

LVAL *builtin_load(LENV *e, LVAL *a)
{
    mpc_result_t r;

    if (mpc_parse_contents(a->cell[0]->str, Lispy, &r))
    {
        LVAL *expr = lval_read(r.output);
        mpc_ast_delete(r.output);

        // TODO: Add a more specific version
        LVAL *x = lval_eval(e, expr);
        lval_println(x);

        return x;
    }
    else
    {
        char *err_msg = mpc_err_string(r.error);
        mpc_err_delete(r.error);

        LVAL *err = lval_err("Could not load lib: %s", err_msg);
        free(err_msg);
        lval_del(a);

        return err;
    }
}