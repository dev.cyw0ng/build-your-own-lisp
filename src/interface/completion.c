#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "completion.h"
#include "string.h"

void init_completion()
{
    rl_attempted_completion_function = completer;
}

char **completer(const char *text, int start, int end)
{
    rl_attempted_completion_over = 1;
    if (g_top_e == NULL || end == 0)
    {
        return NULL;
    }

    char **selected_strs = (char **)malloc((g_top_e->count + 1) * sizeof(char *));
    int selected_count = 0;
    for (int i = 0; i < g_top_e->count; i++)
    {
        if (strstr(g_top_e->syms[i], text) == g_top_e->syms[i])
        {
            *(selected_strs + selected_count) = malloc((strlen(g_top_e->syms[i]) + 1) * sizeof(char));
            memcpy(*(selected_strs + selected_count), g_top_e->syms[i], (strlen(g_top_e->syms[i]) + 1));
            selected_count++;
        }
    }
    if (selected_count == 0)
    {
        return NULL;
    }
    else
    {
        selected_strs[selected_count] = NULL;
        return selected_strs;
    }
}