#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include "lval-ops.h"
#include "cli.h"
#include "syntax.h"
#include "lenv.h"
#include "completion.h"

#ifdef _WIN32

static char buffer[2048];

char *readline(char *prompt)
{
    fputs(prompt, stdout);
    fgets(buffer, 2048, stdin);
    char *cpy = malloc(strlen(buffer) + 1);
    strncpy(cpy, buffer, strlen(buffer));
    cpy[strlen(cpy) - 1] = '\0';

    return cpy;
}

void add_history(char *unused);
#endif

extern char *optarg;
extern int optind, opterr, optopt;

LENV *g_top_e = NULL;
bool g_direct_eval_mode = false;
char *g_direct_eval_cmd = NULL;
static void load_script_from_operand(char *, bool);

int main(int argc, char **argv)
{
    int c;
    bool opt_quiet = false;
    bool opt_script_only = false;

    char *temp_possible_script = NULL;
    g_top_e = lenv_new();
    lenv_add_builtins(g_top_e);
    
    mpc_ast_t *Lispy = syntax_init();
    if (NULL == Lispy)
    {
        puts("Failed to init syntax\n");
        exit(1);
    }

    while ((c = getopt(argc, argv, "qhc:s:l:")) != -1)
    {
        switch (c)
        {
        case 'q':
            opt_quiet = true;
            break;
        case 'h':
            show_help();
            break;
        case 'c':
            g_direct_eval_mode = true;
            if (strlen(optarg) == 0)
            {
                printf("Error: cannot evaluate a null string\n");
                exit(1);
            }
            g_direct_eval_cmd = (char *)malloc(strlen(optarg) + 1);
            strncpy(g_direct_eval_cmd, optarg, strlen(optarg) + 1);
            break;
        case 's':
            temp_possible_script = (char *)malloc(strlen(optarg) + 1);
            strncpy(temp_possible_script, optarg, strlen(optarg) + 1);
            load_script_from_operand(temp_possible_script, true);
            free(temp_possible_script);
            opt_script_only = true;
            break;
        case 'l':
            temp_possible_script = (char *)malloc(strlen(optarg) + 1);
            strncpy(temp_possible_script, optarg, strlen(optarg) + 1);
            load_script_from_operand(temp_possible_script, false);
            free(temp_possible_script);
            break;
        default:
            break;
        }
    }

    if (!opt_quiet && !g_direct_eval_mode && !opt_script_only)
    {
        puts("Lispy Version 0.0.0.0.1");
        puts("Press Ctrl+c to Exit\n");
    }

    if (opt_script_only) {
        goto cleanup;
    }

    if (!g_direct_eval_mode)
    {
        init_completion();
        while (1)
        {
            char *input = readline("lispy> ");
            add_history(input);
            eval_entrance((const mpc_parser_t *)Lispy, g_top_e, input);
            free(input);
        }
    } else {
        eval_entrance((const mpc_parser_t *)Lispy, g_top_e, g_direct_eval_cmd);
        free(g_direct_eval_cmd);
    }

cleanup:
    lenv_del(g_top_e);
    syntax_fini();

    exit(0);
}

static void load_script_from_operand(char *str, bool is_eval_directly) {
    LVAL *args = lval_add(lval_sexpr(), lval_str(str));
    LVAL *ret = builtin_load(g_top_e, args);
    if (!is_eval_directly && ret->type == LVAL_ERR) {
        lval_print(ret);
    }

    lval_del(ret);
}

void show_help()
{
    printf("Help for Lispy\n");
    exit(0);
}

int eval_entrance(const mpc_parser_t *syntax_root, LENV *e, char *eval_target)
{
    mpc_result_t r;
    if (mpc_parse("<stdin>", eval_target, syntax_root, &r))
    {
        LVAL *x = lval_eval(e, lval_read(r.output));
        lval_println(x);
        lval_del(x);
        mpc_ast_delete(r.output);
        return 0;
    }
    else
    {
        mpc_err_print(r.output);
        mpc_err_delete(r.output);
        return 1;
    }
}