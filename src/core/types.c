#include "lval-ops.h"
#include "lenv.h"
#include <stdarg.h>
#include <stdio.h>

inline LVAL *lval_num(long x)
{
    LVAL *v = malloc(sizeof(LVAL));
    v->type = LVAL_NUM;
    v->num = x;

    return v;
}

inline LVAL *lval_err(char *fmt, ...)
{
    LVAL *v = malloc(sizeof(LVAL));
    v->type = LVAL_ERR;

    va_list va;
    va_start(va, fmt);
    v->err = malloc(512);

    vsnprintf(v->err, 511, fmt, va);

    v->err = realloc(v->err, strlen(v->err) + 1);
    va_end(va);

    return v;
}

inline LVAL *lval_sym(char *s)
{
    LVAL *v = malloc(sizeof(LVAL));
    v->type = LVAL_SYM;
    v->sym = malloc(strlen(s) + 1);
    strncpy(v->sym, s, strlen(s) + 1);

    return v;
}

inline LVAL *lval_sexpr(void)
{
    LVAL *v = malloc(sizeof(LVAL));
    v->type = LVAL_SEXPR;
    v->count = 0;
    v->cell = NULL;

    return v;
}

inline LVAL *lval_qexpr(void)
{
    LVAL *v = malloc(sizeof(LVAL));
    v->type = LVAL_QEXPR;
    v->count = 0;
    v->cell = NULL;

    return v;
}

void lval_del(LVAL *v)
{
    switch (v->type)
    {
    case LVAL_FUN:
        if (!v->builtin)
        {
            lenv_del(v->env);
            lval_del(v->formals);
            lval_del(v->body);
        }
        break;
    case LVAL_NUM:
        break;
    case LVAL_ERR:
        free(v->err);
        break;
    case LVAL_SYM:
        free(v->sym);
        break;
    case LVAL_STR:
        free(v->str);
        break;
    case LVAL_QEXPR:
    case LVAL_SEXPR:
        for (int i = 0; i < v->count; i++)
        {
            lval_del(v->cell[i]);
        }

        free(v->cell);
        break;
    }

    free(v);
}

void lval_expr_print(LVAL *v, char open, char close)
{
    putchar(open);
    for (int i = 0; i < v->count; i++)
    {
        lval_print(v->cell[i]);

        if (i != (v->count - 1))
        {
            putchar(' ');
        }
    }
    putchar(close);
}

void lval_print_string(LVAL *v) {
    char *escaped = malloc(strlen(v->str) + 1);
    strncpy(escaped, v-> str, strlen(v->str) + 1);
    escaped = mpcf_escape(escaped);

    printf("\"%s\"", escaped);
    free(escaped);
}

void lval_print(LVAL *v)
{
    switch (v->type)
    {
    case LVAL_NUM:
        printf("%li", v->num);
        break;
    case LVAL_ERR:
        printf("Error: %s", v->err);
        break;
    case LVAL_SYM:
        printf("%s", v->sym);
        break;
    case LVAL_STR:
        printf("\"%s\"", v->str);
        break;
    case LVAL_SEXPR:
        lval_expr_print(v, '(', ')');
        break;
    case LVAL_QEXPR:
        lval_expr_print(v, '{', '}');
        break;
    case LVAL_FUN:
        if (v->builtin)
        {
            printf("<function>");
        }
        else
        {
            printf("(\\ ");
            lval_print(v->formals);
            putchar(' ');
            lval_print(v->body);
            putchar(')');
        }
        break;
    }
}

inline void lval_println(LVAL *v)
{
    lval_print(v);
    putchar('\n');
}

LVAL *lval_read_num(mpc_ast_t *t)
{
    errno = 0;
    long x = strtol(t->contents, NULL, 10);
    return errno != ERANGE ? lval_num(x) : lval_err("invalid number");
}

LVAL *lval_read_str(mpc_ast_t *t) {
    t->contents[strlen(t->contents) - 1] = '\0';
    char *unescaped = malloc(strlen(t->contents + 1) + 1);
    strncpy(unescaped, t->contents + 1, strlen(t->contents + 1) + 1);
    unescaped = mpcf_unescape(unescaped);
    
    LVAL *str = lval_str(unescaped);
    free(unescaped);
    
    return str;
}

LVAL *lval_read(mpc_ast_t *t)
{
    if (strstr(t->tag, "number"))
    {
        return lval_read_num(t);
    }
    if (strstr(t->tag, "symbol"))
    {
        return lval_sym(t->contents);
    }
    if(strstr(t->tag, "string"))
    {
        return lval_read_str(t);
    }

    LVAL *x = NULL;
    if (strncmp(t->tag, ">", 1) == 0)
    {
        x = lval_sexpr();
    }
    if (strstr(t->tag, "sexpr"))
    {
        x = lval_sexpr();
    }
    if (strstr(t->tag, "qexpr"))
    {
        x = lval_qexpr();
    }

    for (int i = 0; i < t->children_num; i++)
    {
        if (strncmp(t->children[i]->contents, "(", 1) == 0)
            continue;
        if (strncmp(t->children[i]->contents, ")", 1) == 0)
            continue;
        if (strncmp(t->children[i]->contents, "{", 1) == 0)
            continue;
        if (strncmp(t->children[i]->contents, "}", 1) == 0)
            continue;
        if (strncmp(t->children[i]->tag, "regex", 5) == 0)
            continue;
        if (strstr(t->children[i]->tag, "comment"))
            continue;

        x = lval_add(x, lval_read(t->children[i]));
    }

    return x;
}

LVAL *lval_add(LVAL *v, LVAL *x)
{
    v->count++;
    v->cell = realloc(v->cell, sizeof(LVAL *) * v->count);
    v->cell[v->count - 1] = x;
    return v;
}

LVAL *lval_fun(lbuiltin func)
{
    LVAL *v = malloc(sizeof(LVAL));
    v->type = LVAL_FUN;
    v->builtin = func;

    return v;
}

LVAL *lval_copy(LVAL *v)
{
    LVAL *x = malloc(sizeof(LVAL));
    x->type = v->type;

    switch (v->type)
    {
    case LVAL_FUN:
        if (v->builtin)
        {
            x->builtin = v->builtin;
        }
        else
        {
            x->builtin = NULL;
            x->env = lenv_copy(v->env);
            x->formals = lval_copy(v->formals);
            x->body = lval_copy(v->body);
        }
        break;
    case LVAL_NUM:
        x->num = v->num;
        break;
    case LVAL_SYM:
        x->sym = malloc(strlen(v->sym) + 1);
        strncpy(x->sym, v->sym, strlen(v->sym) + 1);
        break;
    case LVAL_STR:
        x->str = malloc(strlen(v->str) + 1);
        strncpy(x->str, v->str, strlen(v->str) + 1);
        break;
    case LVAL_SEXPR:
    case LVAL_QEXPR:
        x->count = v->count;
        x->cell = malloc(sizeof(LVAL *) * x->count);
        for (int i = 0; i < x->count; i++)
        {
            x->cell[i] = lval_copy(v->cell[i]);
        }
        break;
    }

    return x;
}

LVAL *lval_lambda(LVAL *formals, LVAL *body)
{
    LVAL *v = malloc(sizeof(LVAL));
    v->type = LVAL_FUN;
    v->builtin = NULL;
    v->env = lenv_new();
    v->formals = formals;
    v->body = body;

    return v;
}

LVAL *lval_str(char *s) {
    LVAL* v = malloc(sizeof(LVAL));
    v->type = LVAL_STR;
    v->str = malloc(strlen(s) + 1);
    strncpy(v->str, s, strlen(s) + 1);

    return v;
}