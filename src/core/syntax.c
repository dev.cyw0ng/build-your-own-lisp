#include "syntax.h"

mpc_parser_t *Number;
mpc_parser_t *Symbol;
mpc_parser_t *String;
mpc_parser_t *Sexpr;
mpc_parser_t *Qexpr;
mpc_parser_t *Comment;
mpc_parser_t *Expr;
mpc_parser_t *Lispy;

mpc_ast_t *syntax_init(void)
{
    /* Create some parsers */
    Number = mpc_new("number");
    Symbol = mpc_new("symbol");
    String = mpc_new("string");
    Sexpr = mpc_new("sexpr");
    Qexpr = mpc_new("qexpr");
    Comment = mpc_new("comment");
    Expr = mpc_new("expr");
    Lispy = mpc_new("lispy");

    /* Define syntax structure */
    mpca_lang(MPCA_LANG_DEFAULT,
    "                                                          \
        number   : /-?[0-9]+/  ;                               \
        string   : /\"(\\\\.|[^\"])*\"/  ;                     \
        symbol   : /[a-zA-Z0-9_+\\-\\*\\/\\\\=<>!&]+/ ;        \
        comment  : /;[^\\r\\n]*/  ;                            \
        sexpr    : '(' <expr>* ')' ;                           \
        qexpr    : '{' <expr>* '}' ;                           \
        expr     : <number> | <symbol> | <sexpr>               \
                 | <qexpr> | <string>  | <comment> ;           \
        lispy    : /^/ <expr>* /$/ ;                           \
    ", Number, String, Symbol, Sexpr, Qexpr, Comment, Expr, Lispy);

    return (mpc_ast_t *)Lispy;
}

void syntax_fini(void)
{
    mpc_cleanup(8, Number, Symbol, String, Sexpr, Qexpr, Expr, Lispy, Comment);
}