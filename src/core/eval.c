#include "lval-ops.h"
#include "lenv.h"

LVAL *lval_eval_sexpr(LENV *e, LVAL *v)
{
    for (int i = 0; i < v->count; i++)
    {
        v->cell[i] = lval_eval(e, v->cell[i]);
    }

    for (int i = 0; i < v->count; i++)
    {
        if (v->cell[i]->type == LVAL_ERR)
        {
            return lval_take(v, i);
        }
    }

    if (v->count == 0)
    {
        return v;
    }

    if (v->count == 1)
    {
        return lval_take(v, 0);
    }

    LVAL *f = lval_pop(v, 0);
    if (f->type != LVAL_FUN)
    {
        lval_del(f);
        lval_del(v);
        return lval_err("S-EXP doesnot start with symbol");
    }

    LVAL *result = lval_call(e, f, v);
    lval_del(f);

    return result;
}

LVAL *lval_eval(LENV *e, LVAL *v)
{
    if (v->type == LVAL_SYM)
    {
        return lenv_get(e, v);
    }
    if (v->type == LVAL_SEXPR)
    {
        return lval_eval_sexpr(e, v);
    }

    return v;
}

LVAL *lval_pop(LVAL *v, int i)
{
    LVAL *x = v->cell[i];

    memmove(&v->cell[i], &v->cell[i + 1], sizeof(LVAL *) * (v->count - i - 1));
    v->count--;
    v->cell = realloc(v->cell, sizeof(LVAL *) * v->count);

    return x;
}

LVAL *lval_take(LVAL *v, int i)
{
    LVAL *x = lval_pop(v, i);
    lval_del(v);

    return x;
}

LVAL *builtin_add(LENV *e, LVAL *a)
{
    return builtin_op(a, "+");
}

LVAL *builtin_sub(LENV *e, LVAL *a)
{
    return builtin_op(a, "-");
}

LVAL *builtin_mul(LENV *e, LVAL *a)
{
    return builtin_op(a, "*");
}

LVAL *builtin_div(LENV *e, LVAL *a)
{
    return builtin_op(a, "/");
}

LVAL *builtin_op(LVAL *a, char *op)
{
    for (int i = 0; i < a->count; i++)
    {
        if (a->cell[i]->type != LVAL_NUM)
        {
            lval_del(a);
            return lval_err("Cannot operator on non-number");
        }
    }

    LVAL *x = lval_pop(a, 0);
    if ((strncmp(op, "-", 1) == 0) && a->count == 0)
    {
        x->num = -x->num;
    }

    while (a->count > 0)
    {
        LVAL *y = lval_pop(a, 0);

        if (strncmp(op, "+", 1) == 0)
        {
            x->num += y->num;
        }
        if (strncmp(op, "-", 1) == 0)
        {
            x->num -= y->num;
        }
        if (strncmp(op, "*", 1) == 0)
        {
            x->num *= y->num;
        }
        if (strncmp(op, "/", 1) == 0)
        {
            if (y->num == 0)
            {
                lval_del(x);
                lval_del(y);
                x = lval_err("Div by 0");
                break;
            }
            x->num /= y->num;
        }
        if (strncmp(op, "%", 1) == 0)
        {
            if (y->num == 0)
            {
                lval_del(x);
                lval_del(y);
                x = lval_err("Div by 0");
                break;
            }
            x->num %= y->num;
        }
        if (strncmp(op, "^", 1) == 0)
        {
            lval_del(x);
            lval_del(y);
            x = lval_err("TODO: to be impl");
            break;
        }
        if (strncmp(op, "min", 3) == 0)
        {
            lval_del(x);
            lval_del(y);
            x = lval_err("TODO: to be impl");
            break;
        }
        if (strncmp(op, "max", 3) == 0)
        {
            x = (x > y ? x : y);
            break;
        }

        lval_del(y);
    }

    lval_del(a);

    return x;
}

LVAL *builtin_var(LENV *e, LVAL *a, char *func)
{
    LASSERT(a, (a->cell[0]->type == LVAL_QEXPR), "Function 'def' passed incorrect type");

    LVAL *syms = a->cell[0];
    for (int i = 0; i < syms->count; i++)
    {
        LASSERT(a, (syms->cell[i]->type == LVAL_SYM), "Function 'def' cannot define non-symbol");
    }

    LASSERT(a, (syms->count == a->count - 1), "Function 'def' cannot define incorrect number of values to symbols");

    for (int i = 0; i < syms->count; i++)
    {
        if (strncmp(func, "def", 3) == 0)
        {
            lenv_def(e, syms->cell[i], a->cell[i + 1]);
        }
        else if (strncmp(func, "=", 1) == 0)
        {
            lenv_put(e, syms->cell[i], a->cell[i + 1]);
        }
    }

    lval_del(a);

    return lval_sexpr();
}

LVAL *builtin_def(LENV *e, LVAL *a)
{
    return builtin_var(e, a, "def");
}

LVAL *builtin_put(LENV *e, LVAL *a)
{
    return builtin_var(e, a, "=");
}

LVAL *builtin_lambda(LENV *e, LVAL *a)
{
    // TODO: Add checkings

    for (int i = 0; i < a->cell[0]->count; i++)
    {
        LASSERT(a, (a->cell[0]->cell[i]->type == LVAL_SYM), "Cannot define non-symbol")
    }

    LVAL *formals = lval_pop(a, 0);
    LVAL *body = lval_pop(a, 0);
    lval_del(a);

    return lval_lambda(formals, body);
}

LVAL *builtin_print(LENV *e, LVAL *a) {
    for(int i = 0; i < a->count; i++) {
        lval_print(a->cell[i]); putchar(' ');
    }

    putchar('\n');
    lval_del(a);

    return lval_sexpr();
}

LVAL *builtin_error(LENV *e, LVAL *a) {
    LVAL *err = lval_err(a->cell[0]->str);

    lval_del(a);
    return err;
}

LVAL *lval_call(LENV *e, LVAL *f, LVAL *a)
{
    if (f->builtin)
    {
        return f->builtin(e, a);
    }

    while (a->count)
    {
        if (f->formals->count == 0)
        {
            lval_del(a);
            return lval_err("Function passwd too many arguments.");
        }

        LVAL *sym = lval_pop(f->formals, 0);

        if (strncmp(sym->sym, "&", 1) == 0)
        {
            if (f->formals->count != 1)
            {
                lval_del(a);
                return lval_err("Function format invalid");
            }

            LVAL *nsym = lval_pop(f->formals, 0);
            lenv_put(f->env, nsym, builtin_list(e, a));
            lval_del(sym);
            lval_del(nsym);
            break;
        }
        LVAL *val = lval_pop(a, 0);

        lenv_put(f->env, sym, val);

        lval_del(sym);
        lval_del(val);
    }

    lval_del(a);
    if (f->formals->count > 0 && strncmp(f->formals->cell[0]->sym, "&", 1) == 0)
    {
        if (f->formals->count != 2)
        {
            return lval_err("Function format invalid");
        }

        lval_del(lval_pop(f->formals, 0));

        LVAL *sym = lval_pop(f->formals, 0);
        LVAL *val = lval_qexpr();

        lenv_put(f->env, sym, val);
        lval_del(sym);
        lval_del(val);
    }

    if (f->formals->count == 0)
    {
        f->env->par = e;
        return builtin_eval(f->env, lval_add(lval_sexpr(), lval_copy(f->body)));
    }
    else
    {
        return lval_copy(f);
    }
}