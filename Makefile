BUILD_ROOT =  $(shell pwd)
THIRD_PARTY_PATH := $(BUILD_ROOT)/thirdparty
BUILD_TARGET_PATH := $(BUILD_ROOT)/build
THREADS := `nproc`
MAKE_CMD := make -j $(THREADS)
MPC_ROOT := $(THIRD_PARTY_PATH)/mpc
CFLAGS := -std=c99 -Wall -ggdb3 -Iinclude
CC:= clang
LIBS := -lmpc -ledit
LINK_LIBS_PATH := -L $(BUILD_TARGET_PATH)
ENABLE_ENG_MACRO := -DENG
ENABLE_SANITIZERS := -fsanitize=address,undefined -fno-omit-frame-pointer

SRCS = $(wildcard ./src/*.c) $(wildcard ./src/*/*.c)

.PHONY : thirdparty libmpc clean

all : thirdparty cli

thirdparty : libmpc

libmpc :
	@test -d $(BUILD_TARGET_PATH) && test -f $(BUILD_TARGET_PATH)/libmpc.a || { \
		test -d ./build || mkdir $(BUILD_TARGET_PATH); \
		cd $(MPC_ROOT); $(MAKE_CMD) -f Makefile all; cd $(BUILD_ROOT); \
		cp -r $(MPC_ROOT)/build/libmpc.a $(BUILD_TARGET_PATH); \
	}

cli : libmpc $(SRCS)
	@test -d ./build || mkdir $(BUILD_TARGET_PATH);
	$(CC) $(CFLAGS) $(SRCS) $(ENABLE_ENG_MACRO) $(LINK_LIBS_PATH) $(ENABLE_SANITIZERS) $(LIBS) -o $(BUILD_TARGET_PATH)/parsing

test : all
	make CC=$(CC) -f test/Makefile

# TODO: Add libmpc clean
clean :
	rm -rf $(BUILD_TARGET_PATH)