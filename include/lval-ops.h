#ifndef _LVAL_OPS_
#define _LVAL_OPS_

#include "../thirdparty/mpc/mpc.h"
#include "types.h"
#include <stdbool.h>

#define LASSERT(args, cond, fmt, ...)              \
    if (!(cond))                                   \
    {                                              \
        LVAL* err = lval_err(fmt, ##__VA_ARGS__);  \
        lval_del(args);                            \
        return err;                                \
    }

LVAL *lval_copy(LVAL *);

LVAL *builtin(LVAL *a, char *);
LVAL *builtin_eval(LENV *, LVAL *);
LVAL *builtin_head(LENV *, LVAL *);
LVAL *builtin_tail(LENV *, LVAL *);
LVAL *builtin_join(LENV *, LVAL *);
LVAL *builtin_list(LENV *, LVAL *);
LVAL *lval_join(LVAL *, LVAL *);

LVAL *lval_num(long);
LVAL *lval_err(char *, ...);
LVAL *lval_sym(char *);
LVAL *lval_sexpr(void);
LVAL *lval_qexpr(void);
LVAL *lval_fun(lbuiltin func);
LVAL *lval_lambda(LVAL* formals, LVAL *body);
LVAL *lval_str(char *);

void lval_del(LVAL *);
LVAL *lval_read_num(mpc_ast_t *);
LVAL *lval_read(mpc_ast_t *);
LVAL *lval_add(LVAL *, LVAL *);
LVAL *lval_call(LENV *, LVAL *, LVAL *);

void lval_expr_print(LVAL *v, char open, char);
void lval_print_string(LVAL *);

void lval_print(LVAL *);
void lval_println(LVAL *);
LVAL *lval_eval_sexpr(LENV *, LVAL *);
LVAL *lval_eval(LENV *, LVAL *);
LVAL *lval_pop(LVAL *, int);
LVAL *lval_take(LVAL *, int);
LVAL *builtin_op(LVAL *, char *);

LVAL eval_op(LVAL, char *, LVAL);
LVAL eval(mpc_ast_t *);

LVAL *builtin_add(LENV *, LVAL *);
LVAL *builtin_sub(LENV *, LVAL *);
LVAL *builtin_mul(LENV *, LVAL *);
LVAL *builtin_div(LENV *, LVAL *);

LVAL *builtin_var(LENV *, LVAL *, char *);

LVAL *builtin_def(LENV *, LVAL *);
LVAL *builtin_put(LENV *, LVAL *);

LVAL *builtin_lambda(LENV *e, LVAL *a);
LVAL *builtin_print(LENV *e, LVAL *a);
LVAL *builtin_error(LENV *e, LVAL *a);
LVAL *builtin_load(LENV *, LVAL *);

LVAL *lval_builtin(lbuiltin);

LVAL* lval_read_str(mpc_ast_t *);


#endif