#ifndef _CONDITION_H_
#define _CONDITION_H_

#include "./types.h"
#include "./lval-ops.h"

LVAL *builtin_gt(LENV *, LVAL *);
LVAL *builtin_lt(LENV *, LVAL *);
LVAL *builtin_ge(LENV *, LVAL *);
LVAL *builtin_le(LENV *, LVAL *);
LVAL *builtin_eq(LENV *, LVAL *);
LVAL *builtin_ne(LENV *, LVAL *);
int lval_eq(LVAL *, LVAL *);
LVAL *builtin_if(LENV *, LVAL *);
LVAL *builtin_cmp(LENV *, LVAL *, char *);

LVAL *builtin_ord(LENV *, LVAL *, char *);

#endif