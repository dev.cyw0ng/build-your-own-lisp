#ifndef _LENV_H_
#define _LENV_H_

#include "types.h"

LENV *lenv_new(void);
void lenv_del(LENV *);
LVAL *lenv_get(LENV *, LVAL *);
LVAL *lenv_put(LENV *, LVAL *, LVAL *);
void lenv_add_builtin(LENV *, char *, lbuiltin);
void lenv_add_builtins(LENV *);
LENV* lenv_copy(LENV *);
void lenv_def(LENV *, LVAL *, LVAL *);

#endif