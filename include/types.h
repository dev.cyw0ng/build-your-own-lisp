#ifndef _TYPES_H_
#define _TYPES_H_

struct _LVAL;
struct _LENV;
typedef struct _LVAL LVAL;
typedef struct _LENV LENV;

typedef LVAL *(*lbuiltin)(LENV *, LVAL *);

/**
 * Lisp Value
 */
struct _LVAL
{
    int type;

    // Basic
    long num;
    char *err;
    char *sym;
    char *str;

    // Function
    lbuiltin builtin;
    LENV *env;
    LVAL *formals;
    LVAL *body;

    // Expression
    int count;
    struct _LVAL **cell;
};

/**
 * Lisp Env
 */
struct _LENV
{
    LENV *par;
    int count;
    char **syms;
    LVAL **vals;
};

/**
 * Lisp Value Type
 */
typedef enum _LVAL_TYPE {
    LVAL_ERR,
    LVAL_NUM,
    LVAL_SYM,
    LVAL_STR,
    LVAL_FUN,
    LVAL_SEXPR,
    LVAL_QEXPR
} LVAL_TYPE;

/**
 * Lisp Error Type
 */
typedef enum _LERR {
    LERR_DIV_ZERO,
    LERR_BAD_OP,
    LERR_BAD_NUM
} LERR;

#endif