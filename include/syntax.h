#ifndef _SYNTAX_H_
#define _SYNTAX_H_

#include "../thirdparty/mpc/mpc.h"

mpc_ast_t *syntax_init(void);
void syntax_fini(void);

#endif