#ifndef _CLI_CMD_H_
#define _CLI_CMD_H_

#ifdef _WIN32

char *readline(char *);
void add_history(char *);

#elif __gnu_linux__

#include <editline/readline.h>
#include <editline/history.h>

#include <unistd.h>
#include <stdbool.h>

int getopt(int argc, char * const argv[], const char *optstring);

void show_help() __attribute__ ((noreturn));
int eval_entrance(const mpc_parser_t *, LENV *, char *);

#endif

#endif