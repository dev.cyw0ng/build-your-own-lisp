#ifndef _COMPLETION_H_
#define _COMPLETION_H_

#include <editline/readline.h>
#include <editline/history.h>

#include <unistd.h>
#include <stdbool.h>

#include "lenv.h"

extern CPPFunction *rl_attempted_completion_function;
extern int rl_attempted_completion_over;

extern LENV *g_top_e;

void init_completion();
char **completer(const char *, int ,int);

#endif